<?php

namespace VkSDK\Infrastructure\Component;

use IntegrationCore\Domain\DataProvider as IntegrationCoreDataProvider;
use Psr\Http\Message as GuzzlePsr;

readonly class StreamAdapter implements IntegrationCoreDataProvider\Stream
{
    public function __construct(
        readonly private GuzzlePsr\StreamInterface $stream,
    ) {
    }

    public function read(int $bytes): string
    {
        return $this->stream->read($bytes);
    }

    public function isEndOfFile(): bool
    {
        return $this->stream->eof();
    }
}
