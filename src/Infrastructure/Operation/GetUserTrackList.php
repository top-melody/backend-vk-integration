<?php

namespace VkSDK\Infrastructure\Operation;

use IntegrationCore\Infrastructure\Enum;
use Psr\Http\Message\ResponseInterface;
use VkSDK\Domain\Enum\RequestUrl;
use VkSDK\Domain\Request;
use VkSDK\Domain\DTO;
use VkSDK\Infrastructure\Operation\Trait as OperationTrait;
use GuzzleHttp;
use IntegrationCore\Infrastructure\Component as IntegrationCoreInfrastructureComponent;

class GetUserTrackList implements IntegrationCoreInfrastructureComponent\IntegrationOperation
{
    use OperationTrait\GetErrors;
    use OperationTrait\GetRawRequest;
    use OperationTrait\GetRawResponse;
    use OperationTrait\GetRequestHeaders;
    use OperationTrait\GetResponseHeaders;
    use OperationTrait\GetResponse;
    use OperationTrait\GetClient;

    public function __construct(
        readonly private DTO\CommonConfig $commonConfig,
        readonly private DTO\UserConfig $userConfig,
    ) {
    }

    private ?Request\AudioGet $request = null;
    private ?ResponseInterface $guzzleResponse = null;

    public function buildRequest(): void
    {
        $this->request = new Request\AudioGet();
        $this->request->access_token = $this->commonConfig->getToken();
        $this->request->owner_id = $this->userConfig->getUserId();
    }

    public function sendRequest(): void
    {
        $this->guzzleResponse = $this->getClient()->post(RequestUrl::AudioGet->getFull(), [
            'headers' => $this->getRequestHeaders(),
            'form_params' => $this->request->toArray(),
        ]);
    }

    public function getType(): Enum\RequestType
    {
        return Enum\RequestType::GetUserTrackList;
    }

    public function getRequestUrl(): ?string
    {
        return RequestUrl::AudioGet->getFull();
    }
}
