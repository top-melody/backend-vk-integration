<?php

namespace VkSDK\Infrastructure\Operation;

use IntegrationCore\Infrastructure\Enum;
use Psr\Http\Message as GuzzlePsr;
use GuzzleHttp\Exception\GuzzleException;
use VkSDK\Infrastructure\Operation\Trait as OperationTrait;
use GuzzleHttp;
use IntegrationCore\Infrastructure\Component as IntegrationCoreInfrastructureComponent;

class GetStream implements IntegrationCoreInfrastructureComponent\IntegrationOperation
{
    use OperationTrait\GetResponseHeaders;
    use OperationTrait\GetClient;

    private const HTTP_STATUS_SUCCESS = 200;

    private ?GuzzlePsr\ResponseInterface $guzzleResponse = null;

    public function __construct(
        readonly private string $downloadUrl,
    ) {
    }

    public function buildRequest(): void
    {
        return;
    }

    /**
     * @throws GuzzleException
     */
    public function sendRequest(): void
    {
        $this->guzzleResponse = $this->getClient()->get($this->downloadUrl, [
            'stream' => true
        ]);
    }

    public function getErrors(): array
    {
        $messages = [];

        $responseCode = $this->guzzleResponse?->getStatusCode();
        if ($responseCode !== self::HTTP_STATUS_SUCCESS) {
            $messages[] = "Неверный статус ответа: {$responseCode}";
        }

        return $messages;
    }

    public function getType(): Enum\RequestType
    {
        return Enum\RequestType::GetStream;
    }

    public function getRequestUrl(): ?string
    {
        return $this->downloadUrl;
    }

    public function getRawRequest(): ?string
    {
        return null;
    }

    public function getRawResponse(): ?string
    {
        return null;
    }

    public function getRequestHeaders(): array
    {
        return [];
    }

    public function getResponse(): ?GuzzlePsr\StreamInterface
    {
        return $this->guzzleResponse?->getBody();
    }
}
