<?php

namespace VkSDK\Infrastructure\Operation;

use IntegrationCore\Infrastructure\Enum;
use Psr\Http\Message\ResponseInterface;
use VkSDK\Domain\DTO;
use VkSDK\Domain\Enum\RequestUrl;
use VkSDK\Domain\Request;
use VkSDK\Infrastructure\Operation\Trait as OperationTrait;
use GuzzleHttp;
use IntegrationCore\Infrastructure\Component as IntegrationCoreInfrastructureComponent;

class GetUserInfo implements IntegrationCoreInfrastructureComponent\IntegrationOperation
{
    use OperationTrait\GetErrors;
    use OperationTrait\GetRawRequest;
    use OperationTrait\GetRawResponse;
    use OperationTrait\GetRequestHeaders;
    use OperationTrait\GetResponseHeaders;
    use OperationTrait\GetResponse;
    use OperationTrait\GetClient;

    public function __construct(
        readonly private DTO\CommonConfig $commonConfig,
        readonly private string $username,
    ) {
    }

    private ?Request\UsersGet $request = null;
    private ?ResponseInterface $guzzleResponse = null;

    public function buildRequest(): void
    {
        $this->request = new Request\UsersGet();
        $this->request->access_token = $this->commonConfig->getToken();
        $this->request->user_ids = $this->username;
    }

    public function sendRequest(): void
    {
        $this->guzzleResponse = $this->getClient()->post(RequestUrl::UsersGet->getFull(), [
            'headers' => $this->getRequestHeaders(),
            'form_params' => $this->request->toArray(),
        ]);
    }

    public function getType(): Enum\RequestType
    {
        return Enum\RequestType::GetUserInfo;
    }

    public function getRequestUrl(): ?string
    {
        return RequestUrl::UsersGet->getFull();
    }
}
