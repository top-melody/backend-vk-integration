<?php

namespace VkSDK\Infrastructure\Operation;

use IntegrationCore\Infrastructure\Enum;
use VkSDK\Domain\DTO\CommonConfig;
use Vodka2\VKAudioToken\TokenFacade;
use GuzzleHttp;
use IntegrationCore\Infrastructure\Component as IntegrationCoreInfrastructureComponent;

class GetToken implements IntegrationCoreInfrastructureComponent\IntegrationOperation
{
    private ?array $response = null;

    public function __construct(
        readonly private CommonConfig $commonConfig,
    ) {
    }

    public function buildRequest(): void
    {
        return;
    }

    public function sendRequest(): void
    {
        $this->response = TokenFacade::getKateToken(
            $this->commonConfig->getUsername(),
            $this->commonConfig->getPassword()
        );
    }

    public function getErrors(): array
    {
        return [];
    }

    public function getType(): Enum\RequestType
    {
        return Enum\RequestType::Auth;
    }

    public function getRequestUrl(): ?string
    {
        return null;
    }

    public function getRawRequest(): ?string
    {
        return json_encode([
            'login' => $this->commonConfig->getUsername(),
            'pass' => $this->commonConfig->getPassword(),
        ]);
    }

    public function getRawResponse(): ?string
    {
        return $this->response ? json_encode($this->response) : null;
    }

    public function getRequestHeaders(): array
    {
        return [];
    }

    public function getResponseHeaders(): array
    {
        return [];
    }

    public function getResponse(): array
    {
        return $this->response;
    }
}
