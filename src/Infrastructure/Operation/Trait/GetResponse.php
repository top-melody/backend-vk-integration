<?php

namespace VkSDK\Infrastructure\Operation\Trait;

trait GetResponse
{
    private ?array $responseArray = null;

    public function getResponse(): array
    {
        if (!$this->responseArray) {
            $responseContent = (string) $this->guzzleResponse?->getBody();
            $this->responseArray = json_decode($responseContent, true);
        }

        return $this->responseArray;
    }
}
