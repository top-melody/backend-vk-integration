<?php

namespace VkSDK\Infrastructure\Operation\Trait;

trait GetResponseHeaders
{
    /**
     * @return array<string, string>
     */
    public function getResponseHeaders(): array
    {
        $result = [];

        $array = $this->guzzleResponse?->getHeaders() ?? [];
        foreach ($array as $name => $valuesArray) {
            $result[$name] = implode(', ', $valuesArray);
        }

        return $result;
    }
}
