<?php

namespace VkSDK\Infrastructure\Operation\Trait;

trait GetErrors
{
    private const HTTP_STATUS_SUCCESS = 200;

    public function getErrors(): array
    {
        $messages = [];

        $responseCode = $this->guzzleResponse?->getStatusCode() ?? null;
        if ($responseCode !== self::HTTP_STATUS_SUCCESS) {
            $messages[] = "Неверный статус ответа: {$responseCode}";
        }

        $errorMessage = $this->getResponse()['error']['error_msg'] ?? null;
        if ($errorMessage) {
            $messages[] = $errorMessage;
        }

        return $messages;
    }
}
