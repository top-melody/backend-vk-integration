<?php

namespace VkSDK\Infrastructure\Operation\Trait;

trait GetRawResponse
{
    public function getRawResponse(): ?string
    {
        $body = $this->guzzleResponse?->getBody() ?? null;
        if (!$body) {
            return null;
        }

        return (string) $body;
    }
}
