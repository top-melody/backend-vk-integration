<?php

namespace VkSDK\Infrastructure\Operation\Trait;

use GuzzleHttp;

trait GetClient
{
    private const HTTP_TIMEOUT_IN_SECONDS = 60;

    private function getClient(): GuzzleHttp\Client
    {
        return new GuzzleHttp\Client([
            'timeout' => self::HTTP_TIMEOUT_IN_SECONDS,
        ]);
    }
}