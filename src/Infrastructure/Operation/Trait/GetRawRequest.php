<?php

namespace VkSDK\Infrastructure\Operation\Trait;

trait GetRawRequest
{
    public function getRawRequest(): ?string
    {
        return $this->request ? json_encode($this->request) : null;
    }
}
