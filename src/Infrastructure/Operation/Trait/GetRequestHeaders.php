<?php

namespace VkSDK\Infrastructure\Operation\Trait;

trait GetRequestHeaders
{
    public function getRequestHeaders(): array
    {
        return [
            'User-Agent' => $this->commonConfig->getUserAgent(),
        ];
    }
}
