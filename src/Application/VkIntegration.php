<?php

namespace VkSDK\Application;

use IntegrationCore\Domain\Component as IntegrationCoreComponent;
use IntegrationCore\Domain\Response as IntegrationCoreResponse;
use VkSDK\Domain\DTO;
use VkSDK\Domain\Service as DomainService;

class VkIntegration implements IntegrationCoreComponent\Integration
{
    private ?DTO\CommonConfig $commonConfig;
    private ?DTO\UserConfig $userConfig;

    public function __construct(array $commonConfig, array $userConfig = [])
    {
        $this->commonConfig = new DTO\CommonConfig($commonConfig);
        $this->userConfig = new DTO\UserConfig($userConfig);
    }

    public function auth(): IntegrationCoreResponse\Auth
    {
        $service = new DomainService\Auth($this->commonConfig);
        return $service->service();
    }

    public function getStream(string $downloadUrl): IntegrationCoreResponse\GetStream
    {
        $service = new DomainService\GetStream($downloadUrl);
        return $service->service();
    }

    /**
     * @return \Generator<IntegrationCoreResponse\TrackList>
     * @throws \IntegrationCore\Domain\Exception\AuthenticationRequired
     * @throws \IntegrationCore\Domain\Exception\UserInfoRequired
     */
    public function getUserTrackList(): \Generator
    {
        $service = new DomainService\GetUserTrackList($this->commonConfig, $this->userConfig);
        yield $service->service();
    }

    /**
     * @throws \IntegrationCore\Domain\Exception\AuthenticationRequired
     */
    public function getUserInfo(): IntegrationCoreResponse\GetUserInfo
    {
        $service = new DomainService\GetUserInfo($this->commonConfig, $this->userConfig);
        return $service->service();
    }
}
