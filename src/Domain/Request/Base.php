<?php

namespace VkSDK\Domain\Request;

abstract class Base
{
    private const DEFAULT_CLIENT_VERSION = 5.95;

    public ?string $access_token = null;

    /**
     * Версия клиента
     */
    public float $v = self::DEFAULT_CLIENT_VERSION;

    public function toArray(): array
    {
        return json_decode(json_encode($this), true);
    }
}
