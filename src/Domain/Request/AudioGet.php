<?php

namespace VkSDK\Domain\Request;

class AudioGet extends Base
{
    /**
     * Идентификатор владельца аудиозаписей (пользователь или сообщество)
     * Если владелец - сообщество, необходимо ставить "-" перед значением
     */
    public ?int $owner_id = null;

    /**
     * Идентификатор альбома с аудиозаписями
     */
    public ?int $album_id = null;

    /**
     * Идентификаторы аудиозаписей, информацию о которых необходимо вернуть
     * @var int[]
     */
    public array $audio_ids = [];

    /**
     * 1 — возвращать информацию о пользователях, загрузивших аудиозапись.
     */
    public int $need_user = 0;

    /**
     * Смещение, необходимое для выборки определенного количества аудиозаписей. По умолчанию — 0.
     * Обратите внимание, что даже с использованием параметра offset получить информацию об аудиозаписях,
     * находящихся после первых 6 тысяч в списке пользователя или сообщества, невозможно.
     */
    public int $offset = 0;

    /**
     * Количество аудиозаписей, информацию о которых необходимо вернуть. Максимальное значение — 6000
     */
    public int $count = 6000;
}
