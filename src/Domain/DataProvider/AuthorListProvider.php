<?php

namespace VkSDK\Domain\DataProvider;

use IntegrationCore\Domain\DataProvider;
use IntegrationCore\Domain\DTO\Author;
use IntegrationCore\Meta\ServiceInterface;

class AuthorListProvider implements ServiceInterface
{
    public function __construct(
        readonly private array $trackArray
    ) {
    }

    /**
     * @return DataProvider\Author[]
     */
    public function service(): array
    {
        $artistNames = array_merge(
            [$this->trackArray['artist'] ?? null],
            array_map(
                fn (array $artist) => $artist['name'] ?? null,
                $this->trackArray['main_artists'] ?? [],
            ),
            array_map(
                fn (array $artist) => $artist['name'] ?? null,
                $this->trackArray['featured_artists'] ?? [],
            ),
        );

        return array_map(
            function (string $authorName) {
                $dto = new Author();
                $dto->name = $authorName;
                return $dto;
            },
            array_unique(
                array_filter($artistNames)
            ),
        );
    }
}
