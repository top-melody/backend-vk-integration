<?php

namespace VkSDK\Domain\DataProvider;

use IntegrationCore\Domain\DataProvider;

class TrackProvider implements DataProvider\Track
{
    public function __construct(
        readonly private array $trackArray,
    ) {
    }

    public function getName(): ?string
    {
        return $this->trackArray['title'] ?? null;
    }

    public function getDuration(): ?int
    {
        $duration = $this->trackArray['duration'] ?? null;
        return $duration ? (int)$duration : null;
    }

    public function getDownloadUrl(): ?string
    {
        return $this->trackArray['url'] ?? null;
    }

    /**
     * @return DataProvider\Author[]
     */
    public function getAuthors(): array
    {
        $provider = new AuthorListProvider($this->trackArray);
        return $provider->service();
    }
}
