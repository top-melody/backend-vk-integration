<?php

namespace VkSDK\Domain\DataProvider;

use IntegrationCore\Domain\DTO\Response as IntegrationCoreResponseDTO;
use IntegrationCore\Domain\Response as IntegrationCoreResponse;

class TrackListProvider extends IntegrationCoreResponseDTO\Base implements IntegrationCoreResponse\TrackList
{
    public function __construct(
        readonly private array $audioGetResponse,
    ) {
    }

    public function getTrackList(): array
    {
        return array_map(
            fn (array $track) => new TrackProvider($track),
            $this->audioGetResponse['response']['items'] ?? [],
        );
    }
}
