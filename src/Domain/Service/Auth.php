<?php

namespace VkSDK\Domain\Service;

use IntegrationCore\Domain\DataProvider\Log;
use IntegrationCore\Infrastructure\Component\IntegrationOperation;
use IntegrationCore\Meta\ServiceInterface;
use VkSDK\Domain\DTO;
use VkSDK\Domain\Exception as DomainException;
use VkSDK\Infrastructure\Operation;
use IntegrationCore\Domain\Service as IntegrationCoreService;
use IntegrationCore\Domain\Response as IntegrationCoreResponse;
use IntegrationCore\Domain\DTO\Response as IntegrationCoreResponseDTO;
use Vodka2\VKAudioToken\SupportedClients;

class Auth extends BaseOperationService
{
    public function __construct(
        readonly private DTO\CommonConfig $commonConfig,
    ) {
    }

    public function service(): IntegrationCoreResponse\Auth
    {
        if (!$this->commonConfig->getUsername() || !$this->commonConfig->getPassword()) {
            $this->errors[] = 'Необходимо заполнить авторизационные данные';
            return $this->buildResult();
        }

        try {
            $operation = new Operation\GetToken($this->commonConfig);
            $response = $this->executeOperation($operation);

            $this->updateCommonConfig($response);
        } catch (DomainException\OperationFail $exception) {
            // Do nothing
        }

        return $this->buildResult();
    }

    private function updateCommonConfig(array $response): void
    {
        $this->commonConfig->setToken($response['token']);
        $this->commonConfig->setUserAgent($response['userAgent']);
    }

    private function buildResult(): IntegrationCoreResponse\Auth
    {
        $result = new IntegrationCoreResponseDTO\Auth();
        $result->commonConfig = $this->commonConfig->toArray();
        $result->logList = $this->logs;
        $result->errorList = $this->errors;

        return $result;
    }
}
