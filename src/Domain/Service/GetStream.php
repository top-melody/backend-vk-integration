<?php

namespace VkSDK\Domain\Service;

use IntegrationCore\Domain\Service as IntegrationCoreService;
use IntegrationCore\Domain\Response as IntegrationCoreResponse;
use IntegrationCore\Domain\DTO\Response as IntegrationCoreResponseDTO;
use IntegrationCore\Meta\ServiceInterface;
use VkSDK\Infrastructure\Operation;
use VkSDK\Infrastructure\Component as InfrastructureComponent;
use IntegrationCore\Domain\DTO;
use VkSDK\Domain\Exception as DomainException;
use Psr\Http\Message as GuzzlePsr;

class GetStream extends BaseOperationService
{
    public function __construct(
        readonly private string $downloadUrl,
    ) {
    }

    public function service(): IntegrationCoreResponse\GetStream
    {
        try {
            $operation = new Operation\GetStream($this->getMp3DownloadUrl());
            /** @var GuzzlePsr\StreamInterface|null $operationResult */
            $operationResult = $this->executeOperation($operation);
            if (!($operationResult instanceof GuzzlePsr\StreamInterface)) {
                $this->errors[] = 'В ответе не стрим';
                throw new DomainException\OperationFail();
            }

            return $this->buildSuccessResult($operationResult);
        } catch (DomainException\OperationFail $exception) {
            return $this->buildFailResult();
        }
    }

    private function getMp3DownloadUrl(): string
    {
        if (!strpos($this->downloadUrl, "index.m3u8?")) {
            return $this->downloadUrl;
        }

        if (str_contains($this->downloadUrl, "/audios/")) {
            return preg_replace('~^(.+?)/[^/]+?/audios/([^/]+)/.+$~', '\\1/audios/\\2.mp3', $this->downloadUrl);
        }

        return preg_replace('~^(.+?)/(p[0-9]+)/[^/]+?/([^/]+)/.+$~', '\\1/\\2/\\3.mp3', $this->downloadUrl);
    }

    private function buildSuccessResult(GuzzlePsr\StreamInterface $stream): IntegrationCoreResponseDTO\GetStream
    {
        $sizeInBytes = ($this->responseHeaders['Content-Length'] ?? false)
            ? intval($this->responseHeaders['Content-Length'])
            : null;
        $mimeType = $this->responseHeaders['Content-Type'] ?? null;

        $result = new IntegrationCoreResponseDTO\GetStream();
        $result->errorList = $this->errors;
        $result->logList = $this->logs;
        $result->sizeInBytes = $sizeInBytes;
        $result->mimeType = $mimeType;
        $result->stream = new InfrastructureComponent\StreamAdapter($stream);

        return $result;
    }

    private function buildFailResult(): IntegrationCoreResponseDTO\GetStream
    {
        $result = new IntegrationCoreResponseDTO\GetStream();
        $result->logList = $this->logs;
        $result->errorList = $this->errors;

        return $result;
    }
}
