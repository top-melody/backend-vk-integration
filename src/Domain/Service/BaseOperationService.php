<?php

namespace VkSDK\Domain\Service;

use IntegrationCore\Domain\DataProvider\Log;
use IntegrationCore\Domain\Exception\AuthenticationRequired;
use IntegrationCore\Infrastructure\Component\IntegrationOperation;
use IntegrationCore\Meta\ServiceInterface;
use VkSDK\Domain\Exception as DomainException;
use IntegrationCore\Domain\Service as IntegrationCoreService;

abstract class BaseOperationService implements ServiceInterface
{
    private const ERROR_CODE_ACCESS_TOKEN_HAS_EXPIRED = 5;

    /**
     * @var string[]
     */
    protected array $errors = [];

    /**
     * @var Log[]
     */
    protected array $logs = [];

    /**
     * @var array<string, string>
     */
    protected array $responseHeaders = [];

    /**
     * @throws DomainException\OperationFail
     */
    protected function executeOperation(IntegrationOperation $operation): mixed
    {
        $operationExecutor = new IntegrationCoreService\IntegrationOperationExecutor($operation);
        $operationResult = $operationExecutor->service();
        $this->logs[] = $operationResult->getLog();
        $this->responseHeaders = $operationResult->getLog()->getResponseHeaders();

        if ($operationResult->getLog()->getErrors()) {
            $response = $operationResult->getResponse();
            if (
                is_array($response)
                && ($errorCode = $response['error']['error_code'] ?? null)
                && $errorCode == self::ERROR_CODE_ACCESS_TOKEN_HAS_EXPIRED
            ) {
                throw new AuthenticationRequired($this->logs);
            }

            $this->errors = $operationResult->getLog()->getErrors();
            throw new DomainException\OperationFail();
        }

        return $operationResult->getResponse();
    }
}
