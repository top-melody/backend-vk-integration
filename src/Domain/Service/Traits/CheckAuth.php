<?php

namespace VkSDK\Domain\Service\Traits;

use IntegrationCore\Domain\Exception\AuthenticationRequired;
use VkSDK\Domain\DTO\CommonConfig;

trait CheckAuth
{
    /**
     * @throws AuthenticationRequired
     */
    private function checkAuth(CommonConfig $commonConfig): void
    {
        if (!$commonConfig->getToken() || !$commonConfig->getUserAgent()) {
            throw new AuthenticationRequired();
        }
    }
}