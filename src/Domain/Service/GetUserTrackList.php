<?php

namespace VkSDK\Domain\Service;

use IntegrationCore\Domain\Exception\AuthenticationRequired;
use IntegrationCore\Domain\Exception\UserInfoRequired;
use IntegrationCore\Domain\Response as IntegrationCoreResponse;
use IntegrationCore\Infrastructure\Component\IntegrationOperation;
use IntegrationCore\Meta\ServiceInterface;
use VkSDK\Domain\DTO;
use IntegrationCore\Domain\Service as IntegrationCoreService;
use VkSDK\Domain\Exception\OperationFail;
use VkSDK\Domain\Service\Traits;
use VkSDK\Infrastructure\Operation;
use IntegrationCore\Domain\DTO\Response as IntegrationCoreResponseDTO;
use VkSDK\Domain\DataProvider;

class GetUserTrackList extends BaseOperationService
{
    use Traits\CheckAuth;

    public function __construct(
        readonly private DTO\CommonConfig $commonConfig,
        readonly private DTO\UserConfig $userConfig
    ) {
    }

    /**
     * @throws AuthenticationRequired
     * @throws UserInfoRequired
     */
    public function service(): IntegrationCoreResponse\TrackList
    {
        $this->checkAuth($this->commonConfig);

        if (!$this->userConfig->getUserId()) {
            throw new UserInfoRequired();
        }

        try {
            $operation = new Operation\GetUserTrackList($this->commonConfig, $this->userConfig);
            $response = $this->executeOperation($operation);

            return $this->buildSuccessResult($response);
        } catch (OperationFail $operationFail) {
            return $this->buildFailResult();
        }
    }

    private function buildSuccessResult(array $operationResult): IntegrationCoreResponse\TrackList
    {
        $result = new DataProvider\TrackListProvider($operationResult);
        $result->logList = $this->logs;
        $result->errorList = $this->errors;

        return $result;
    }

    private function buildFailResult(): IntegrationCoreResponse\TrackList
    {
        $result = new IntegrationCoreResponseDTO\TrackList();
        $result->logList = $this->logs;
        $result->errorList = $this->errors;

        return $result;
    }
}
