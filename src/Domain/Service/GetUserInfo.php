<?php

namespace VkSDK\Domain\Service;

use IntegrationCore\Domain\DataProvider;
use IntegrationCore\Infrastructure\Component\IntegrationOperation;
use VkSDK\Domain\DTO;
use IntegrationCore\Domain\Response as IntegrationCoreResponse;
use IntegrationCore\Domain\DTO\Response as IntegrationCoreResponseDTO;
use VkSDK\Domain\Service\Traits;
use VkSDK\Infrastructure\Operation;
use IntegrationCore\Meta\ServiceInterface;
use VkSDK\Domain\Exception as DomainException;
use IntegrationCore\Domain\Service as IntegrationCoreService;
use IntegrationCore\Domain\Exception as IntegrationCoreException;

class GetUserInfo extends BaseOperationService
{
    use Traits\CheckAuth;

    public function __construct(
        readonly private DTO\CommonConfig $commonConfig,
        private DTO\UserConfig $userConfig,
    ) {
    }

    /**
     * @throws IntegrationCoreException\AuthenticationRequired
     */
    public function service(): IntegrationCoreResponse\GetUserInfo
    {
        $this->checkAuth($this->commonConfig);

        if (!$this->userConfig->getUserUrl()) {
            $this->errors[] = 'Необходимо заполнить url пользователя';
            return $this->buildResult();
        }

        $username = $this->getUsernameFromUrl($this->userConfig->getUserUrl());

        try {
            $operation = new Operation\GetUserInfo($this->commonConfig, $username);
            $response = $this->executeOperation($operation);
        } catch (DomainException\OperationFail) {
            return $this->buildResult();
        }

        $userId = $response['response'][0]['id'] ?? null;
        if (!$userId) {
            $this->errors[] = 'Не получен id пользователя по url';
            return $this->buildResult();
        }

        $this->userConfig->setUserId($userId);
        return $this->buildResult();
    }

    private function getUsernameFromUrl(string $url): string
    {
        $explodedUrl = explode('/', $url);
        return end($explodedUrl);
    }

    private function buildResult(): IntegrationCoreResponse\GetUserInfo
    {
        $result = new IntegrationCoreResponseDTO\GetUserInfo();
        $result->userConfig = $this->userConfig->toArray();
        $result->logList = $this->logs;
        $result->errorList = $this->errors;

        return $result;
    }
}
