<?php

namespace VkSDK\Domain\Enum;

enum RequestUrl: string
{
    private const BASE = 'https://api.vk.com';

    case AudioGet = '/method/audio.get/';
    case UsersGet = '/method/users.get/';

    public function getFull(): string
    {
        return $this->getDomain() . $this->value;
    }
    
    private function getDomain(): string
    {
        return getenv('VK_API_BASE_URL') ?: self::BASE;
    }
}
