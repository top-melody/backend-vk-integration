<?php

namespace VkSDK\Domain\DTO;

use IntegrationCore\Domain\DataProvider;
use IntegrationCore\Domain\DTO;

class UserConfig extends DTO\UserConfigDTO
{
    public function getUserId(): ?int
    {
        return $this->userConfig['userId'] ?? null;
    }

    public function setUserId(?int $userId): self
    {
        $this->userConfig['userId'] = $userId;

        return $this;
    }
}
