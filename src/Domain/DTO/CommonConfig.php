<?php

namespace VkSDK\Domain\DTO;

class CommonConfig
{
    public function __construct(
        private array $config = [],
    ) {
    }

    public function getUsername(): ?string
    {
        return $this->config['username'] ?? null;
    }

    public function setUsername(?string $username): self
    {
        $this->config['username'] = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->config['password'] ?? null;
    }

    public function setPassword(?string $password): self
    {
        $this->config['password'] = $password;

        return $this;
    }

    public function getUserAgent(): ?string
    {
        return $this->config['userAgent'] ?? null;
    }

    public function setUserAgent(?string $userAgent): self
    {
        $this->config['userAgent'] = $userAgent;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->config['token'] ?? null;
    }

    public function setToken(?string $token): self
    {
        $this->config['token'] = $token;

        return $this;
    }

    public function toArray(): array
    {
        return $this->config;
    }
}
