<?php

namespace VkSDKTests\Functional;

use IntegrationCore\Domain\Exception\AuthenticationRequired;
use IntegrationCore\Domain\Exception\UserInfoRequired;
use IntegrationCore\Domain\Response as IntegrationCoreResponse;
use PHPUnit\Framework\TestCase;
use VkSDK\Application\VkIntegration;
use VkSDK\Domain\DTO\CommonConfig;
use VkSDK\Domain\DTO\UserConfig;
use VkSDKTestsComponents\Traits;

class GetUserTrackListTest extends TestCase
{
    use Traits\MockServerResponse;
    use Traits\ReadDataFile;

    public function testSuccess(): void
    {
        $response = $this->readDataFile('AudioGet/SuccessResponse.json');
        $this->mockServerResponse(200, $response);

        $commonConfig = new CommonConfig();
        $commonConfig->setToken('token');
        $commonConfig->setUserAgent('userAgent');

        $userConfig = new UserConfig();
        $userConfig->setUserId(12345);

        $facade = new VkIntegration($commonConfig->toArray(), $userConfig->toArray());
        $response = $facade->getUserTrackList();
        /** @var IntegrationCoreResponse\TrackList $firstIteration */
        $firstIteration = $response->current();

        $this->assertTrue($firstIteration->getSuccess());
        $this->assertCount(0, $firstIteration->getErrorList());
        $this->assertCount(1, $firstIteration->getLogList());

        $firstTrack = current($firstIteration->getTrackList());
        $this->assertNotNull($firstTrack->getName());
        $this->assertNotNull($firstTrack->getDuration());
        $this->assertNotNull($firstTrack->getDownloadUrl());
        $this->assertTrue((bool)$firstTrack->getAuthors());

        $firstAuthor = current($firstTrack->getAuthors());
        $this->assertNotNull($firstAuthor->getName());
    }

    public function testNeedAuth(): void
    {
        $userConfig = new UserConfig();
        $userConfig->setUserId(12345);

        $this->expectException(AuthenticationRequired::class);

        $facade = new VkIntegration([], $userConfig->toArray());
        $response = $facade->getUserTrackList();
        $response->current();
    }

    public function testNeedUserInfo(): void
    {
        $commonConfig = new CommonConfig();
        $commonConfig->setToken('token');
        $commonConfig->setUserAgent('userAgent');

        $this->expectException(UserInfoRequired::class);

        $facade = new VkIntegration($commonConfig->toArray(), []);
        $response = $facade->getUserTrackList();
        $response->current();
    }

    public function testAccessTokenHasExpired(): void
    {
        $response = $this->readDataFile('AccessTokenHasExpiredResponse.json');
        $this->mockServerResponse(200, $response);

        $commonConfig = new CommonConfig();
        $commonConfig->setToken('token');
        $commonConfig->setUserAgent('userAgent');

        $userConfig = new UserConfig();
        $userConfig->setUserId(12345);

        $this->expectException(AuthenticationRequired::class);

        $facade = new VkIntegration($commonConfig->toArray(), $userConfig->toArray());
        $result = $facade->getUserTrackList();
        $result->current();
    }
}