<?php

namespace VkSDKTests\Functional;

use GuzzleHttp\Psr7\ServerRequest;
use GuzzleHttp\Server\Server;
use IntegrationCore\Domain\Exception\AuthenticationRequired;
use PHPUnit\Framework\TestCase;
use VkSDK\Application\VkIntegration;
use VkSDK\Domain\DTO\CommonConfig;
use VkSDK\Domain\DTO\UserConfig;
use GuzzleHttp\Psr7\Response;
use VkSDKTestsComponents\Traits;

class GetUserInfoTest extends TestCase
{
    use Traits\MockServerResponse;
    use Traits\ReadDataFile;

    private const REAL_USER_URL = 'https://vk.com/id146590181';

    public function testSuccess(): void
    {
        $response = $this->readDataFile('UsersGet/SuccessResponse.json');
        $this->mockServerResponse(200, $response);

        $commonConfig = new CommonConfig();
        $commonConfig->setToken('token');
        $commonConfig->setUserAgent('userAgent');

        $userConfig = new UserConfig();
        $userConfig->setUserUrl(self::REAL_USER_URL);

        $facade = new VkIntegration($commonConfig->toArray(), $userConfig->toArray());
        $result = $facade->getUserInfo();
        $userConfig = new UserConfig($result->getUserConfig());

        $this->assertTrue($result->getSuccess());
        $this->assertCount(0, $result->getErrorList());
        $this->assertCount(1, $result->getLogList());
        $this->assertTrue(str_contains(self::REAL_USER_URL, $userConfig->getUserId()));
    }

    public function testEmptyUserUrl(): void
    {
        $commonConfig = new CommonConfig();
        $commonConfig->setToken('token');
        $commonConfig->setUserAgent('userAgent');

        $facade = new VkIntegration($commonConfig->toArray(), []);
        $result = $facade->getUserInfo();

        $this->assertFalse($result->getSuccess());
        $this->assertCount(1, $result->getErrorList());
        $this->assertCount(0, $result->getLogList());
    }

    public function testNeedAuth(): void
    {
        $userConfig = new UserConfig();
        $userConfig->setUserUrl(self::REAL_USER_URL);

        $this->expectException(AuthenticationRequired::class);

        $facade = new VkIntegration([], $userConfig->toArray());
        $result = $facade->getUserInfo();
    }

    public function testAccessTokenHasExpired(): void
    {
        $response = $this->readDataFile('AccessTokenHasExpiredResponse.json');
        $this->mockServerResponse(200, $response);

        $commonConfig = new CommonConfig();
        $commonConfig->setToken('token');
        $commonConfig->setUserAgent('userAgent');

        $userConfig = new UserConfig();
        $userConfig->setUserUrl(self::REAL_USER_URL);

        $this->expectException(AuthenticationRequired::class);

        $facade = new VkIntegration($commonConfig->toArray(), $userConfig->toArray());
        $result = $facade->getUserInfo();
    }
}