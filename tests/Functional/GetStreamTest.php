<?php

namespace VkSDKTests\Functional;

use IntegrationCore\Domain\DataProvider as IntegrationCoreDataProvider;
use PHPUnit\Framework\TestCase;
use VkSDK\Application\VkIntegration;
use VkSDK\Domain\DTO\CommonConfig;
use VkSDKTestsComponents\Traits;

class GetStreamTest extends TestCase
{
    use Traits\MockServerResponse;

    private const CONTENT_LENGTH = 12345;
    private const CONTENT_TYPE = 'audio/mpeg';

    public function testSuccess(): void
    {
        $serverUrl = $this->mockServerResponse(200, '', [
            'Content-Length' => self::CONTENT_LENGTH,
            'Content-Type' => self::CONTENT_TYPE,
        ]);

        $facade = new VkIntegration([], []);
        $result = $facade->getStream($serverUrl);

        $this->assertTrue($result->getSuccess());
        $this->assertCount(0, $result->getErrorList());
        $this->assertEquals(self::CONTENT_LENGTH, $result->getSizeInBytes());
        $this->assertEquals(self::CONTENT_TYPE, $result->getMimeType());
        $this->assertTrue($result->getStream() instanceof IntegrationCoreDataProvider\Stream);
    }
}