<?php

namespace VkSDKTestsComponents\Traits;

trait ReadDataFile
{
    private function readDataFile(string $path): string
    {
        return file_get_contents(__DIR__ . '/../Data/' . $path);
    }
}