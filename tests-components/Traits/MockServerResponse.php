<?php

namespace VkSDKTestsComponents\Traits;

use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Server\Server;

trait MockServerResponse
{
    /**
     * @return string serverUrl
     * @throws \Exception
     */
    private function mockServerResponse(int $status, string $response, array $headers = []): string
    {
        Server::start();
        register_shutdown_function(static function () {
            Server::stop();
        });

        putenv(sprintf("VK_API_BASE_URL=%s", Server::$url));

        Server::enqueue([
            new Response($status, $headers, $response),
        ]);

        return Server::$url;
    }
}