default: start

start:
	docker-compose up -d;
stop:
	docker-compose down;
build:
	docker-compose build;
code-fix:
	docker run --rm -v `pwd`/src:/data/src cytopia/php-cs-fixer fix .
composer-install:
	docker exec -it vk-php composer install
composer-update-core:
	docker exec -it vk-php composer update top-melody/backend-integration-core
test-all:
	docker exec -it vk-php vendor/bin/phpunit tests/